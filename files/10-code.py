from base import get_input

pairs = {')': '(', ']': '[', '}': '{', '>': '<'}

def calc_syntax_error_score(lines):
    points = {')': 3, ']': 57, '}': 1197, '>': 25137}
    score = 0
    for line in lines:
        score += points.get(find_illegal_char(line), 0)

    return score


def find_illegal_char(line):
    opens = [None]
    for char in line:
        expected = pairs.get(char, None)
        if expected:
            actual = opens.pop()
            if expected != actual:
                return char
        else:
            opens.append(char)


def filter_corrupted_lines(lines):
    # TODO: use filter with lambda
    filtered = []
    for line in lines:
        if not find_illegal_char(line):
            filtered.append(line)
    return filtered


def find_unpaired_opens(line):
    opens = []
    for char in line:
        if pairs.get(char, None):
            opens.pop()
        else:
            opens.append(char)

    opens.reverse()
    return opens


def calc_line_complete_score(line):
    points = {'(': 1, '[': 2, '{': 3, '<': 4}
    score = 0
    opens = find_unpaired_opens(line)
    if not opens:
        return score

    for char in opens:
        score = score * 5 + points[char]
    return score


def find_winning_complete_score(lines):
    lines = filter_corrupted_lines(lines)
    scores = []
    for line in lines:
        line_score = calc_line_complete_score(line)
        if line_score > 0:
            scores.append(line_score)

    scores.sort()
    return scores[len(scores)//2]


if __name__ == '__main__':
    intext = ['[({(<(())[]>[[{[]{<()<>>',
              '[(()[<>])]({[<{<<[]>>(',
              '{([(<{}[<>[]}>{[]{[(<()>',
              '(((({<>}<{<{<>}{[]{[]{}',
              '[[<[([]))<([[{}[[()]]]',
              '[{[{({}]{}}([{[{{{}}([]',
              '{<[[]]>}<{[{[{[]{()[[[]',
              '[<(<(<(<{}))><([]([]()',
              '<{([([[(<>()){}]>(<<{{',
              '<{([{{}}[<[[[<>{}]]]>[]]']

    assert calc_syntax_error_score([intext[2]]) == 1197
    assert calc_syntax_error_score(intext) == 26397
    assert len(filter_corrupted_lines(intext)) == 5
    assert calc_line_complete_score('<{([{{}}[<[[[<>{}]]]>[]]') == 294
    assert find_winning_complete_score(intext) == 288957

    intext = get_input('10-input.txt')
    print(f"Part 1: {calc_syntax_error_score(intext)}")      # 369105
    print(f"Part 2: {find_winning_complete_score(intext)}")  # 3999363569
