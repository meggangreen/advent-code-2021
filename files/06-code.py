from base import get_input

# first pattern: num fish 2x every 7 days
# eg 5: 10, 20, 40, 80 => * 2**n -- nope
# remaining fish with clock < remaining days will spawn

# second pattern: fish clock = inital - 6 + 8
# sort initial by day ascending -- don't want to sort 5000+ vals
# calculate clock for new
# stick on to end

# def get_week_school_len(initial, days):
#     return len(initial) * 2**int(days / 7)


# new plan: dict clock_val: num_fish

def get_initial_school(intext):
    return [int(n) for n in intext.split(",")]


def make_blank_school():
    """Fishies is clock_val: num_fishes """
    return {clock: 0 for clock in range(9)}


def fill_initial_school(initial):
    fishies = make_blank_school()
    for clock in initial:
        fishies[clock] += 1

    return fishies


def sim_one_week(fishies):
    # after one week:
    # for each fish with clock <= 6, +1 fish at clock+2 (clock -6+8)
    # for each fish with clock > 6, -1 fish at clock, +1 fish at clock-7
    week_of_fish = make_blank_school()

    for clock in fishies:
        if clock <= 6:
            week_of_fish[clock+2] += 1 * fishies[clock]
        else:
            week_of_fish[clock] -= 1 * fishies[clock]
            week_of_fish[clock-7] += 1 * fishies[clock]

    for clock in fishies:
        fishies[clock] += week_of_fish[clock]

    return fishies


def sim_partial_week(fishies, days):
    # adj clock based on days
    # move fish
    # add new fish
    days_of_fish = make_blank_school()

    for clock in fishies:
        if clock >= days:
            adj_clock = clock - days
            new_fish_clock = None
        else:
            adj_clock = clock - days + 7
            new_fish_clock = clock - days + 9
        days_of_fish[adj_clock] += fishies[clock]
        if new_fish_clock:
            days_of_fish[new_fish_clock] += fishies[clock]

    return days_of_fish


def sim_fishies(intext, days):
    fishies = fill_initial_school(get_initial_school(intext))
    weeks = int(days/7)
    days = int(days%7)
    for w in range(weeks):
        fishies = sim_one_week(fishies)
    fishies = sim_partial_week(fishies, days)

    return fishies

if __name__ == '__main__':
    intext = "3,4,3,1,2"

    initial = get_initial_school(intext)
    assert initial == [3,4,3,1,2]
    fishies = fill_initial_school(initial)
    assert fishies == {0: 0, 1: 1, 2: 1, 3: 2, 4: 1, 5: 0, 6: 0, 7: 0, 8: 0}

    day_7_of_fish = fill_initial_school([3,4,3,1,2,3,4,5,5,6])
    fishies = sim_one_week(fishies)
    assert fishies == day_7_of_fish
    day_8_of_fish = fill_initial_school([2,3,2,0,1,2,3,4,4,5])
    assert sim_partial_week(fishies, 1) == day_8_of_fish
    day_10_of_fish = fill_initial_school([0,1,0,5,6,0,1,2,2,3,7,8])
    assert sim_partial_week(fishies, 3) == day_10_of_fish

    fishies = sim_fishies(intext, 14)
    day_14_of_fish = fill_initial_school([3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8])
    assert fishies == day_14_of_fish

    fishies = sim_fishies(intext, 18)
    day_18_of_fish = fill_initial_school([6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8])
    assert fishies == day_18_of_fish
    assert sum(fishies.values()) == 26

    fishies = sim_fishies(intext, 80)
    assert sum(fishies.values()) == 5934

    intext = get_input("06-input.txt")[0]
    fishies = sim_fishies(intext, 80)
    print(f"Part 1: {sum(fishies.values())}")  # 387413
    fishies = sim_fishies(intext, 256)
    print(f"Part 2: {sum(fishies.values())}")  # 1738377086345
