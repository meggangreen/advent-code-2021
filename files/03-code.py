from base import get_input

def get_power_consumption(diagnostics):
    diags = convert_diags(diagnostics)
    gamma_bits = calc_gamma_rate(diags)
    epsilon_bits = [0 if b == 1 else 1 for b in gamma_bits]
    gamma = int(''.join(str(b) for b in gamma_bits), 2)
    epsilon = int(''.join(str(b) for b in epsilon_bits), 2)

    return gamma * epsilon


def get_life_support_rating(diagnostics):
    diags = convert_diags(diagnostics)
    oxy_rate = calc_gas_rate(diags)
    co2_rate = calc_gas_rate(diags, True)

    return oxy_rate * co2_rate


def convert_diags(diagnostics):
    return [[int(b) for b in bits] for bits in diagnostics]


def calc_gamma_rate(diags):
    gamma = [0] * len(diags[0])
    for i in range(len(gamma)):
        gamma[i] = calc_most_common_bit(diags, i)

    return gamma


def calc_gas_rate(diags, reverse=False):
    for i in range(len(diags[0])):
        bit = calc_most_common_bit(diags, i)
        if reverse is True:
            bit = 0 if bit == 1 else 1
        diags = filter_diags(diags, i, bit)
        if len(diags) == 1:
            return int(''.join(str(b) for b in diags[0]), 2)


def calc_most_common_bit(diags, i):
    return 0 if sum([diag[i] for diag in diags]) < len(diags)/2 else 1


def filter_diags(diags, i, bit):
    filtered_diags = []
    for diag in diags:
        if diag[i] == bit:
            filtered_diags.append(diag)

    return filtered_diags


if __name__ == '__main__':
    intext = ['00100', '11110', '10110', '10111', '10101', '01111', '00111', '11100', '10000', '11001', '00010', '01010']
    assert get_power_consumption(intext) == 198
    assert get_life_support_rating(intext) == 230

    intext = get_input('03-input.txt')
    print(f"Power Consumption is {get_power_consumption(intext)}")
    print(f"Life Support Rating is {get_life_support_rating(intext)}")
