from base import get_input

class Board(object):
    """docstring for Board"""

    def __init__(self, grid):
        self.bingo = False
        self.__make_groups(grid)
        self.__calc_score()

    def __make_groups(self, grid):
        self.groups = []
        for g in range(10):
            self.groups.append(Group())
        for row in range(5):
            for col in range(5):
                num = grid[row][col]
                self.groups[row].add_num(num)
                self.groups[col+5].add_num(num)

    def __calc_score(self):
        self.score = 0
        for group in self.groups[:5]:
            self.score += sum(group.nums['not called'])

    def process_num(self, num):
        for group in self.groups:
            group.process_num(num)
            self.bingo = group.is_bingo()
            if self.bingo:
                self.__calc_score()
                return self.bingo


class Group(object):
    """docstring for Group"""

    def __init__(self):
        # self.bingo = False
        self.nums = {"not called": set(), "called": set()}

    def add_num(self, num):
        self.nums["not called"].add(num)

    def process_num(self, num):
        if num in self.nums["not called"]:
            self.nums["not called"].remove(num)
            self.nums["called"].add(num)

    def is_bingo(self):
        return len(self.nums["called"]) == 5


def prepare_input(intext):
    nums = [int(n) for n in intext[0].split(',')]

    grids = []
    line = 2
    while line < len(intext):
        grid = []
        for i in range(line, line+5):
            grid.append([int(g) for g in intext[i].split()])
        grids.append(grid)
        line += 6

    return nums, grids


def make_boards(grids):
    boards = []
    for grid in grids:
        boards.append(Board(grid))

    return boards


def find_first_winner_score(nums, grids):

    boards = make_boards(grids)

    bingo = False
    for num in nums:
        for board in boards:
            board.process_num(num)
            if board.bingo:
                return board.score * num


def find_last_winner_score(nums, grids):

    boards = make_boards(grids)
    result = None

    bingo = False
    for num in nums:
        for board in boards:
            if not board.bingo:
                board.process_num(num)
                if board.bingo:
                    result = board.score * num

    return result


if __name__ == '__main__':
    intext = get_input("04-test.txt")
    nums, grids = prepare_input(intext)
    assert find_first_winner_score(nums, grids) == 4512
    assert find_last_winner_score(nums, grids) == 1924

    intext = get_input("04-input.txt")
    nums, grids = prepare_input(intext)
    print(f"Part 1: {find_first_winner_score(nums, grids)}")
    print(f"Part 2: {find_last_winner_score(nums, grids)}")
