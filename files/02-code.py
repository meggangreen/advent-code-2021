from base import get_input

def get_position_one(movements, coord=0+0j):

    for move in movements:
        steer, val = move[0], int(move[-1])
        if steer == 'u':
            coord += val * 1j
        elif steer == 'd':
            coord += val * -1j
        else: #F
            coord += val

    return coord


def get_position_two(movements, coord=0+0j):

    aim = 0
    for move in movements:
        steer, val = move[0], int(move[-1])
        if steer == 'u':
            aim += val
        elif steer == 'd':
            aim -= val
        else: #F
            coord += val + val * aim * 1j

    return coord


if __name__ == '__main__':
    intext = ['forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2']
    assert get_position_one(intext) == 15-10j

    intext = ['forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2']
    assert get_position_two(intext) == 15-60j

    intext = get_input('02-input.txt')
    coord = get_position_one(intext)
    print(f"Position: {coord}; Product: {coord.real * coord.imag}")
    coord = get_position_two(intext)
    print(f"Position: {coord}; Product: {coord.real * coord.imag}")
