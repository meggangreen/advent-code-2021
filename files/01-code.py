from base import get_input

def traverse_depths(intext):
    count = 0
    for i in range(1, len(intext)):
        if int(intext[i]) > int(intext[i-1]):
            count += 1
    return count


def traverse_win_depths(intext):
    count = 0
    for i in range(3, len(intext)):
        curr_win = sum([int(intext[i]), int(intext[i-1]), int(intext[i-2])])
        prev_win = sum([int(intext[i-1]), int(intext[i-2]), int(intext[i-3])])
        if curr_win > prev_win:
            count += 1
    return count


if __name__ == '__main__':
    intext = ['1', '3', '7', '5']
    assert traverse_depths(intext) == 2

    intext = ['2', '2', '4', '6','1','0']
    assert traverse_win_depths(intext) == 1

    intext = get_input("01-input.txt")
    print(f"Increases: {traverse_depths(intext)}")
    print(f"Window Increases: {traverse_win_depths(intext)}")
