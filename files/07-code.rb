def get_input_file(filename)
    File.read(filename)
        .split(',')
        .map(&:to_i)
end


def herd_crabs_simple(intext)
    least_fuel = Float::INFINITY
    min_h = intext.min
    max_h = intext.max
    (min_h..max_h).each do |h, fuel=0|
        intext.each { |crab| fuel += (h-crab).abs }
        least_fuel = fuel < least_fuel ? fuel : least_fuel
    end
    least_fuel
end


def herd_crabs_complex(intext)
    least_fuel = Float::INFINITY
    min_h = intext.min
    max_h = intext.max
    (min_h..max_h).each do |h, fuel=0|
        intext.each { |crab| fuel += (1..(h-crab).abs).sum }
        least_fuel = fuel < least_fuel ? fuel : least_fuel
    end
    least_fuel
end

if __FILE__ == $0
    # test func; disabled for testing input
    # intext = get_input_file("07-input.txt")
    # puts intext == [16,1,2,0,4,2,7,1,2,14]

    intext = [16,1,2,0,4,2,7,1,2,14]
    puts herd_crabs_simple(intext) == 37
    puts herd_crabs_complex(intext) == 168

    intext = get_input_file("07-input.txt")
    puts herd_crabs_simple(intext)  # 328262
    puts herd_crabs_complex(intext)  # 90040997
end
