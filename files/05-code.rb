def get_input_file(filename)
    File.readlines(filename).map { |line| line.strip! }
end


def make_segments(intext, filter=false)
    re = Regexp.new(/(?<x1>\d+),(?<y1>\d+) -> (?<x2>\d+),(?<y2>\d+)/)

    segments = []
    intext.each do |text|
        m = text.match(re)
        if (filter && (m[:x1] == m[:x2]) || (m[:y1] == m[:y2])) || (not filter)
            segments << [m[:x1].to_i, m[:y1].to_i, m[:x2].to_i, m[:y2].to_i]
        end
    end
    segments
end


def plot_points(segments)
    points = Hash.new(0)
    segments.each do |x1, y1, x2, y2|
        x, y = [x1, x2].min == x1 ? [x1, y1] : [x2, y2]
        x_step = 1
        length = (x1-x2).abs
        if ((x1 < x2) && (y1 < y2)) || ((x1 > x2) && (y1 > y2))
            y_step = 1
        elsif ((x1 < x2) && (y1 > y2) || (x1 > x2) && (y1 < y2))
            y_step = -1
        elsif y1 == y2
            y_step = 0
        elsif x1 == x2
            x_step = 0
            length = (y1-y2).abs
            y_step = 1
        end

        i = 0
        # puts [x1, y1, x2, y2]
        until i > length do
            point = Complex(x,y)
            # puts point
            points[point] += 1
            x += x_step
            y += y_step
            i += 1
        end
    end
    points
end


def count_danger(intext, filter=false)
    # a point is dangerous if 2+ lines cross it
    segments = make_segments(intext, filter)
    points = plot_points(segments)
    points.size - points.values.count(1)
end


def count_all_danger(intext)

end


if __FILE__ == $0
    intext =   ["0,9 -> 5,9",
                "8,0 -> 0,8",
                "9,4 -> 3,4",
                "2,2 -> 2,1",
                "7,0 -> 7,4",
                "6,4 -> 2,0",
                "0,9 -> 2,9",
                "3,4 -> 1,4",
                "0,0 -> 8,8",
                "5,5 -> 8,2"]


    puts count_danger(intext, filter=true) == 5
    puts count_danger(intext) == 12

    intext = get_input_file("05-input.txt")
    puts "Part 1: #{count_danger(intext, filter=true)}"  # 5092

end
