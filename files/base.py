def get_input(filename):
    with open(filename) as intext:
        return [line.strip() for line in intext.readlines()]
