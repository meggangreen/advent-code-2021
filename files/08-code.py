import re
from base import get_input

def count_1478(intext):
    l1478 = [2, 3, 4, 7]
    segment_p = re.compile(r"\w+")
    count = 0
    for line in intext:
        for segment in segment_p.findall(line)[-4:]:
            count += 1 if len(segment) in l1478 else 0

    return count


def decipher_digits(segments):
    locs = {loc: None for loc in ['tc', 'tl', 'tr', 'mc', 'bl', 'br', 'bc']}
    digits = {n: None for n in range(10)}
    digits[8] = set('abcdefg')
    # 1 = cf, 7 = Acf, 4 = bcdf, 3 = ADGcf(B), 5 = ABDFG(CE)

    for seg in segments:
        if len(seg) == 2:
            digits[1] = seg
            break

    for seg in segments:
        if len(seg) == 3:
            digits[7] = seg
            locs['tc'] = seg - digits[1]
            break

    for seg in segments:
        if len(seg) == 4:
            digits[4] = seg
            break

    for seg in segments:
        if (len(seg) == 5) & (seg > digits[7]):
            digits[3] = seg
            locs['bc'] = digits[3] - digits[7] - digits[4]
            locs['mc'] = digits[3] - digits[7] - locs['bc']
            locs['tl'] = digits[4] - digits[3]
            break

    for seg in segments:
        if (len(seg) == 5) & (seg > locs['tl']):
            digits[5] = seg
            locs['tr'] = digits[1] - digits[5]
            locs['br'] = digits[1] - locs['tr']
            locs['bl'] = digits[8] - digits[1] - digits[5]
            break

    digits[0] = digits[8] - locs['mc']
    digits[2] = digits[8] - locs['tl'] - locs['br']
    digits[6] = digits[8] - locs['tr']
    digits[9] = digits[8] - locs['bl']

    return digits


def segments_to_digits(line):
    segments = [set(m) for m in re.findall(r"\w+", line)]
    digits = decipher_digits(segments)
    output = segments[-4:]
    for i in range(4):
        for digit in digits:
            if set(output[i]) == digits[digit]:
                output[i] = str(digit)
                break

    return int(''.join(output))


def sum_all_outputs(intext):
    total = 0
    for line in intext:
        total += segments_to_digits(line)

    return total

if __name__ == '__main__':
    intext =   ["be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe",
                "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc",
                "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg",
                "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb",
                "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea",
                "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb",
                "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe",
                "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef",
                "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb",
                "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"]

    assert count_1478(intext) == 26

    line = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
    segments = [set(m) for m in re.findall(r"\w+", line)]
    expected_digits =   {0: {'a', 'b', 'c', 'd', 'e', 'g'},
                         1: {'a', 'b'},
                         2: {'a', 'c', 'd', 'f', 'g'},
                         3: {'a', 'b', 'c', 'd', 'f'},
                         4: {'a', 'b', 'e', 'f'},
                         5: {'b', 'c', 'd', 'e', 'f'},
                         6: {'b', 'c', 'd', 'e', 'f', 'g'},
                         7: {'a', 'b', 'd'},
                         8: {'a', 'b', 'c', 'd', 'e', 'f', 'g'},
                         9: {'a', 'b', 'c', 'd', 'e', 'f'}}

    assert decipher_digits(segments) == expected_digits

    assert sum_all_outputs(intext) == 61229

    intext = get_input("08-input.txt")
    print(f"Part 1: {count_1478(intext)}")  # 294
    print(f"Part 2: {sum_all_outputs(intext)}")  #973292
