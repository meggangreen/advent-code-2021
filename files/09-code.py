"""
cannot think of a solution that doesn't involve looking at every point 1 to 5 times
    i can think of many ways to look 1 to 5 times, so best to go with simplist

ways to reduce loops?
    '0' is auto low point -- or lowest low? if we didn't know there would be 0s and 9s
    '9' is auto skip -- or highest high?
"""

from base import get_input

class Point(object):
    """ docstring for Point """

    def __init__(self, coords, elevation):
        self.coords = coords
        self.elevation = elevation


class Grid(object):
    """ docstring for Grid """

    def __init__(self, data):
        self.data = data
        self.height = len(self.data)
        self.width = len(self.data[0])
        self.graph = None
        self.lowest_points = None

    def find_lowest_points(self):
        self.lowest_points = set()
        for row in range(self.height):
            for col in range(self.width):
                if self._is_lowest_neighbor(row, col):
                    self.lowest_points.add(Point((row, col), self.data[row][col]))

    def calc_total_risk(self):
        return sum(list(map(lambda p: p.elevation+1, self.lowest_points)))

    def _is_lowest_neighbor(self, row, col):
        return all(
                   self.data[row][col] < self.data[x][y]
                   for i, j in ((-1, 0), (1, 0), (0, -1), (0, 1))
                   if 0 <= (x := row + i) < self.height and 0 <= (y := col + j) < self.width
                   )


def ingest_file(file):
    return [[int(n) for n in line] for line in get_input(file)]


if __name__ == '__main__':
    file = '09-input.txt'
    data = ingest_file(file)
    grid = Grid(data)
    grid.find_lowest_points()  # len = 209
    print(f"Part 1: {grid.calc_total_risk()}")  # 512
    # Part 2 is 1600104
