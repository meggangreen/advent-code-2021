""" https://www.reddit.com/r/adventofcode/comments/rca6vp/comment/ho4k4ie/ """

from math import prod
import fileinput

m = [list(map(int, line.strip())) for line in fileinput.input('09-input.txt')]
h, w, part1, part2 = len(m), len(m[0]), 0, []

# print(h, w, part1, part2)
adjacents = ((-1, 0), (1, 0), (0, -1), (0, 1))

# import pdb; pdb.set_trace()
for row in range(h):
    for col in range(w):
        # so the 'if any()' below says:
        # adj_is_lower = []  # technically it makes a generator not a list
        # for i, j in adjacents:
        #     if 0 <= (x := row + i) < h and 0 <= (y := col + j) < w:  # if the adjacents are in bounds, set x,y
        #         if m[row][col] >= m[x][y]:
        #             adj_is_lower.append(True)
        #             # could exit early here with 'break', because we only need one True (not in 'any' below)
        # if True in adj_is_lower:
        #     continue

        if any(
            m[row][col] >= m[x][y]
            for i, j in adjacents
            if 0 <= (x := row + i) < h and 0 <= (y := col + j) < w
        ):
            continue

        part1 += m[row][col] + 1

        visited, visiting = set(), set([(row, col)])

        while visiting:
            a, b = visiting.pop()
            visited.add((a, b))

            for i, j in (-1, 0), (1, 0), (0, -1), (0, 1):
                if 0 <= (x := a + i) < h and 0 <= (y := b + j) < w \
                    and m[x][y] < 9 \
                    and (x, y) not in visited:
                    visiting.add((x, y))

        part2.append(len(visited))

print(part1)
print(prod(sorted(part2)[-3:]))
